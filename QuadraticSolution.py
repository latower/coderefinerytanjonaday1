# encoding: utf-8
"""
@author:
@contact:
@time: 2/15/21
@file: QuadraticSolution.py
@desc: To run, type:
    $ python QuadraticSolution.py
"""

import math

def quadratic_solution (a,b,c):
    if a==0:
        print('Enter a non zero first coefficient.')
    else:
        D= b**2-4*a*c
        if D>0:
            X1= (-b-math.sqrt(D))/(2*a)
            X2= (-b+math.sqrt(D))/(2*a)
            print('There are 2 solutions:')
            print (X1)
            print (X2)
            print(" Ther are two solutions X1 = ",X1, "and X2 = ", X2)
      
        elif D==0:
            X= -b/(2*a)
            print ('There is a one solution:')
            print (X)
    
        else:
            print ('There is no real solution.')
    return